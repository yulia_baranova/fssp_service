#!flask/bin/python
from flask import Flask, jsonify, request, abort

from app import models

app = Flask(__name__)
app.config.from_object('config')


@app.route('/api/get_docs', methods=['GET'])
def get_docs():
    return jsonify({'ids': [row[0] for row in models.Document.query.with_entities(models.Document.id).all()]})


@app.route('/api/get_doc', methods=['GET'])
def get_doc():
    doc_id = request.args.get('id')
    if not doc_id:
        abort(500)
    result = models.Document.query.get(doc_id)
    if not result:
        abort(404)
    return jsonify(result.serialize)


if __name__ == '__main__':
    app.run()
