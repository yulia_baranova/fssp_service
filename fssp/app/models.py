from app import db


def dump_date(value):
    """Deserialize datetime object into string form for JSON processing."""
    if value is None:
        return None
    return value.strftime("%Y-%m-%d")


class Document(db.Model):
    __tablename__ = 'enforcment_proceedings'
    __table_args__ = {'sqlite_autoincrement': True}

    id = db.Column(db.Integer, primary_key=True)
    debtor_name = db.Column(db.String(256))
    debtor_addr = db.Column(db.String(256))
    enf_number = db.Column(db.String(50))
    proceeding_date = db.Column(db.Date())
    enf_total_number = db.Column(db.String(50))
    doc_type = db.Column(db.String(50))
    doc_date = db.Column(db.Date())
    doc_object = db.Column(db.String(256))
    exec_doc_object = db.Column(db.String(256))
    exec_object = db.Column(db.String(256))
    paid_amount = db.Column(db.Integer())
    bailiffs_dept = db.Column(db.String(256))
    bailiffs_dept_addr = db.Column(db.String(256))

    def __init__(self, debtor_name, debtor_addr, enf_number, proceeding_date, enf_total_number, doc_type, doc_date, doc_object,
                 exec_doc_object, exec_object, paid_amount, bailiffs_dept, bailiffs_dept_addr):
        self.debtor_name = debtor_name
        self.debtor_addr = debtor_addr
        self.enf_number = enf_number
        self.proceeding_date = proceeding_date
        self.enf_total_number = enf_total_number
        self.doc_type = doc_type
        self.doc_date = doc_date
        self.doc_object = doc_object
        self.exec_doc_object = exec_doc_object
        self.exec_object = exec_object
        self.paid_amount = paid_amount
        self.bailiffs_dept = bailiffs_dept
        self.bailiffs_dept_addr = bailiffs_dept_addr

    def __repr__(self):
        return "The name of the debtor='%s', The number of enforcement proceeding='%s', Amount to be paid='%s')" % (
            self.debtor_name, self.enf_number, self.paid_amount)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'debtor_name': self.debtor_name,
            'debtor_addr': self.debtor_addr,
            'enf_number': self.enf_number,
            'proceeding_date': dump_date(self.proceeding_date),
            'enf_total_number': self.enf_total_number,
            'doc_type': self.doc_type,
            'doc_date': dump_date(self.doc_date),
            'doc_object': self.doc_object,
            'exec_doc_object': self.exec_doc_object,
            'exec_object': self.exec_object,
            'paid_amount': self.paid_amount,
            'bailiffs_dept': self.bailiffs_dept,
            'bailiffs_dept_add': self.bailiffs_dept_addr
        }