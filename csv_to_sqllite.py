import csv
from datetime import datetime
from sqlalchemy import Column, Integer, String, Date, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


FIELD_NAMES = ['debtor_name', 'debtor_addr', 'enf_number', 'proceeding_date', 'enf_total_number', 'doc_type', 'doc_date',
               'doc_object', 'exec_doc_object', 'exec_object', 'paid_amount', 'bailiffs_dept', 'bailiffs_dept_addr']
Base = declarative_base()


class Document(Base):
    __tablename__ = 'enforcment_proceedings'
    __table_args__ = {'sqlite_autoincrement': True}

    id = Column(Integer, primary_key=True)
    debtor_name = Column(String)
    debtor_addr = Column(String)
    enf_number = Column(String)
    proceeding_date = Column(Date)
    enf_total_number = Column(String)
    doc_type = Column(String)
    doc_date = Column(Date)
    doc_object = Column(String)
    exec_doc_object = Column(String)
    exec_object = Column(String)
    paid_amount = Column(Integer)
    bailiffs_dept = Column(String)
    bailiffs_dept_addr = Column(String)

if __name__ == "__main__":

    def convert_to_date(date_str):
        return datetime.strptime(date_str, '%Y-%m-%d').date() if date_str else None

    engine = create_engine('sqlite:///app.db')
    Base.metadata.create_all(engine)

    session = sessionmaker()
    session.configure(bind=engine)
    s = session()

    try:
        with open('data.csv') as f:
            next(f)  # skip header
            rows = csv.reader(f, delimiter=',')
            for i, row in enumerate(rows):
                row = [x.decode('utf8') for x in row]
                row[3], row[6] = convert_to_date(row[3]), convert_to_date(row[6])  # allowed only Date objects
                s.add(Document(**{key: value for key, value in zip(FIELD_NAMES, row)}))
                if not i % 1000:
                    s.commit()
                    print 'Documents count: %i' % i
    except:
        s.rollback()
    finally:
        s.close()