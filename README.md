csv_to_sqlite.py - parse and load data from Federal Bailiffs Service about executive proceedings in respect of legal entities to SQLiteDB.

fssp folder - a little Flask API to serve data from SQLiteDB  with two methods:
	- /api/get_docs - get all loaded documents ids in JSON format
	- /api/get_doc?id=<id> - get content of the document with the current id
